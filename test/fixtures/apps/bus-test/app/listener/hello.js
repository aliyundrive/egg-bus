'use strict';

class HelloListener {
  static get watch() {
    return [ 'boot' ];
  }

  run(data) {
    const { logger } = this.ctx;
    logger.info(`run ${data.name}`);

    return 2020;
  }

  completed(data, result) {
    console.log(`completed data ${data.name}`);
    console.log(`completed result ${result}`);
  }
}

module.exports = HelloListener;

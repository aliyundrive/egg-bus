'use strict';

const _ = require('lodash');
const bull = require('./bull');
const assert = require('assert');
const job = require('./job');
const listener = require('./listener');
const { merge } = require('lodash');
const { LISTENER_TARGET, JOB_TARGET } = require('./common');

const getInstanceAndData = (app, ctx, job) => {
  let instance = null;
  let data = null;

  if (_.isEmpty(job.data)) {
    return {};
  }

  switch (job.data.type) {
    case 'event': {
      let Listener = app[LISTENER_TARGET];
      job.data.file.split('.').forEach(name => {
        Listener = Listener[name];
      });

      instance = new Listener(app, ctx);
      data = {
        name: job.data.name,
        data: job.data.payload,
      };
      break;
    }
    case 'job': {
      let Job = app[JOB_TARGET];
      job.data.name.split('.').forEach(name => {
        Job = Job[name];
      });

      instance = new Job(app, ctx);
      data = job.data.payload;
      break;
    }
    default:
      throw new Error(`Unknonw job type, job ${JSON.stringify(job)}`);
  }

  return { instance, data };
};

module.exports = app => {
  const config = app.config.bus;
  const ctx = app.createAnonymousContext();

  const { events, listeners } = listener.load(app, config);
  const jobs = job.load(app, config);

  // 合并 listener 和 job 的队列
  const queues = Object.create(null);
  Array.from(new Set([
    config.queue.default,
    ...listeners.map(_ => _.queue),
    ...jobs.map(_ => _.queue),
  ])).forEach(queue => {
    const originalName = queue.replace(config.queue.prefix + ':', '');
    queues[queue] = bull.create(
      app,
      queue,
      merge({}, config.bull, config.queues[originalName])
    );

    const q = queues[queue];

    q.on('failed', async (job, err) => {
      const { instance, data } = getInstanceAndData(app, ctx, job);

      if (job.attemptsMade < job.data.attempts) {
        if (instance === undefined || instance.error === undefined) return;
        await instance.error(data, err, job)
        return;
      }

      if (instance === undefined || instance.failed === undefined) return;

      await instance.failed(data, err, job);
    });

    q.on('completed', async (job, result) => {
      const { instance, data } = getInstanceAndData(app, ctx, job);

      if (instance === undefined || instance.completed === undefined) return;

      await instance.completed(data, result, job);
    });

    const { concurrency: qc } = config.queues[originalName] || {};
    const concurrency = qc || config.concurrency;

    q.process(concurrency, job => {
      const { instance, data } = getInstanceAndData(app, ctx, job);

      if (instance === undefined) return;

      try {
        return instance.run(data, job);
      } catch (error) {
        app.coreLogger.error(`[egg-bus] ${job.data.type === 'event' ? 'listener' : 'job'} error: %s`, error);
        return Promise.reject(error);
      }
    });
  });

  const emit = (name, payload, options) => {
    const listeners = events[name];

    if (!listeners) {
      if (config.debug) {
        app.coreLogger.warn(`[egg-bus] event ${name} has no listeners.`);
      }

      return;
    }

    for (const listener of listeners) {
      const conf = merge(
        {},
        config.job.options,
        { attempts: listener.attempts || config.listener.options.attempts },
        options
      );

      config.debug && app.logger.info(
        `[egg-bus:event] ${name}: ${listener.name} use (${listener.queue})`
      );

      queues[listener.queue].add({
        type: 'event',
        file: listener.name,
        attempts: conf.attempts,
        name,
        payload,
      }, conf);
    }
  };

  const dispatch = (name, payload, options) => {
    const job = jobs.find(_ => _.name === name);
    assert(!!job, `[egg-bus] job ${name} does not exist.`);

    const conf = merge(
      {},
      config.job.options,
      { attempts: job.attempts || config.job.options.attempts },
      options
    );

    config.debug && app.logger.info(`[egg-bus:job] ${name} use (${job.queue})`);

    queues[job.queue].add({
      type: 'job',
      name,
      attempts: conf.attempts,
      payload,
    }, conf);
  };

  const get = name => queues[name];

  app.bus = { listeners, events, jobs, queues, get, emit, dispatch };
};
